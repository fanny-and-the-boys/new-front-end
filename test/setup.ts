import "@testing-library/jest-dom";
import { afterAll, afterEach, beforeAll, vi } from "vitest";
import server from "@test/dependencies/server";
import { mockReactRouter } from "@test/dependencies/mocks/react-router.ts";

// setup mocks
mockReactRouter();

beforeAll(() => {
	server.listen();
	// Mock Date.now() to return a fixed timestamp
	vi.useFakeTimers({
		shouldAdvanceTime: true,
		toFake: ["Date"],
	});
});

afterEach(() => {
	server.resetHandlers();
});

afterAll(() => {
	server.close();
	// Restore the original implementation of Date.now()
	vi.useRealTimers();
});
