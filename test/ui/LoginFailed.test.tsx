import "@testing-library/jest-dom";
import { LoginUseCase } from "@src/domain/usecases/LoginUseCase";
import { render, screen, waitFor } from "@testing-library/react";
import { mock, mockReset } from "vitest-mock-extended";
import userEvent from "@testing-library/user-event";
import { LoginForm } from "@src/ui/components";

describe("Login success / fail test", () => {
	const loginUseCase = mock<LoginUseCase>();

	const submitForm = () => {
		const user = userEvent.setup();
		user.click(screen.getByRole("button"));
	};

	beforeEach(() => {
		mockReset(loginUseCase);
		render(<LoginForm loginUseCase={loginUseCase} />);
		submitForm();
	});

	it("An error message should be displayed in case of a failed login", () => {
		loginUseCase.login.mockRejectedValue(false);
		waitFor(async () => {
			await screen.findByTestId("error-message");
		});
	});

	it("No error message should be displayed in case of a successful login", () => {
		loginUseCase.login.mockRejectedValue(true);
		waitFor(() => {
			const errorMessage = screen.queryByTestId("error-message");
			expect(errorMessage).not.toBeInTheDocument();
		});
	});
});
