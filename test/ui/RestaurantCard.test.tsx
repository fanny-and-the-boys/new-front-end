import { render, screen } from "@testing-library/react";
import { RestaurantCard } from "@src/ui/components";
import { restaurantsSimple } from "@test/dependencies/server/handlers/restaurant.ts";
import { BrowserRouter } from "react-router-dom";

describe("Restaurant card component test", () => {
	const restaurant = restaurantsSimple[0];

	beforeEach(() => {
		render(
			<BrowserRouter>
				<RestaurantCard restaurant={restaurant}></RestaurantCard>
			</BrowserRouter>,
		);
	});

	it("Should display restaurant's name", () => {
		const title = screen.getByRole("heading");
		expect(title.textContent).toBe(restaurant.name);
		expect(title).toBeInTheDocument();
	});

	it("Should display restaurant's image", () => {
		const image = screen.getByRole("img");
		const imageUrl = image.getAttribute("src");
		expect(imageUrl).toBe(restaurant.imageUrl);
		expect(image).toBeInTheDocument();
	});
});
