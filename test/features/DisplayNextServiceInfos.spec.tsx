import { bookingUseCase } from "@src/UseCaseFactory";
import { BookingCard } from "@src/ui/components/dashboard";
import { render, screen, waitFor } from "@testing-library/react";
import { RouterProvider, createMemoryRouter } from "react-router-dom";

describe("As a Restaurant Owner, I want to see my next service infos on my dashboard", async () => {
	it("should display the next service infos", async () => {
		const routes = [
			{
				path: "/",
				element: <BookingCard />,
				loader: async () => {
					return bookingUseCase.getNextBookings();
				},
			},
		];

		render(
			<RouterProvider
				router={createMemoryRouter(routes, {
					initialEntries: ["/"],
				})}
			/>,
		);

		await waitFor(async () => {
			expect(
				await screen.findByTestId("bookingTables"),
			).toBeInTheDocument();
			expect(
				await screen.findByTestId("bookingGuests"),
			).toHaveTextContent("2");
		});
	});
	it("should display a message when no service is found", async () => {
		const routes = [
			{
				path: "/",
				element: <BookingCard />,
			},
		];

		render(
			<RouterProvider
				router={createMemoryRouter(routes, {
					initialEntries: ["/"],
				})}
			/>,
		);

		await waitFor(async () => {
			expect(
				await screen.findByTestId("no-bookings"),
			).toBeInTheDocument();
		});
	});
});
