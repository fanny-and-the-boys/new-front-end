import { render, screen, waitFor } from "@testing-library/react";
import { RouterProvider, createMemoryRouter } from "react-router-dom";
import { RestaurantList } from "@src/ui/components/restaurants";
import { restaurantUseCase } from "@src/UseCaseFactory";
describe("As a Client, I want to see Restaurants where I can book a table", async () => {
	it("should display a list of restaurants", async () => {
		const routes = [
			{
				path: "/",
				element: <RestaurantList />,
				loader: async () => {
					return restaurantUseCase.getAllRestaurants();
				},
			},
		];
		render(
			<RouterProvider
				router={createMemoryRouter(routes, {
					initialEntries: ["/"],
				})}
			/>,
		);
		await waitFor(async () => {
			expect(screen.getByTestId("restaurants")).toBeInTheDocument();
		});
	});

	it("should display a message when no restaurants are found", async () => {
		const routes = [
			{
				path: "/",
				element: <RestaurantList />,
			},
		];
		render(
			<RouterProvider
				router={createMemoryRouter(routes, {
					initialEntries: ["/"],
				})}
			/>,
		);
		await waitFor(async () => {
			expect(screen.getByTestId("no-restaurants")).toBeInTheDocument();
		});
	});
});
