import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { LoginGateway } from "@src/adapters/gateways";
import { JWTRepository } from "@src/adapters/repositories";
import { LoginUseCase } from "@src/domain/usecases";
import { mockedUseNavigate } from "@test/dependencies/mocks/react-router.ts";
import LoginPage from "@pages/LoginPage.tsx";

describe("As a Restaurant Owner, I want to be able to login with my credentials.", async () => {
	const user = userEvent.setup();
	const loginGateway = new LoginGateway();
	const jwtRepository = new JWTRepository();
	const loginUseCase = new LoginUseCase(loginGateway, jwtRepository);

	const loginCredentials = {
		login: "TesterLogin",
		password: "TesterPassword",
	};

	const submitLoginForm = () => {
		user.type(screen.getByTestId("login"), loginCredentials.login);
		user.type(screen.getByTestId("password"), loginCredentials.password);
		user.click(screen.getByRole("button"));
	};

	beforeEach(async () => {
		render(<LoginPage loginUseCase={loginUseCase} />);
	});

	it("should not be submitted without any credentials", () => {
		// Given
		const loginButton = screen.getByRole("button");

		// When
		user.click(loginButton);

		// Then
		waitFor(() => expect(loginUseCase.login).not.toBeCalled());
	});

	it("should not be submitted without a login", () => {
		// Given
		const passwordInput = screen.getByTestId("password");
		const loginButton = screen.getByRole("button");

		// When
		user.type(passwordInput, loginCredentials.password);
		user.click(loginButton);

		// Then
		waitFor(() => expect(loginUseCase.login).not.toBeCalled());
	});

	it("should not be submitted without a password", () => {
		// Given
		const loginInput = screen.getByTestId("login");
		const loginButton = screen.getByRole("button");

		// When
		user.type(loginInput, loginCredentials.login);
		user.click(loginButton);

		// Then
		waitFor(() => expect(loginUseCase.login).not.toBeCalled());
	});

	it("should be submitted with valid credentials", () => {
		// When
		submitLoginForm();

		// Then
		waitFor(() => {
			expect(loginUseCase.login).toBeCalledWith(loginCredentials);
			// Expect to be redirected to dashboard page
			expect(mockedUseNavigate).toBeCalledWith("/dashboard");
		});
	});
});
