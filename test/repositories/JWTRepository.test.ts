import { JWTRepository } from "@src/adapters/repositories";

describe("JWT repository tests", () => {
	let repository: JWTRepository;
	const expectedToken =
		"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

	beforeEach(() => {
		repository = new JWTRepository();
	});

	it("Should throw an error when requesting a token that is not set", () => {
		const getToken = () => repository.getToken();
		expect(getToken).toThrowError();
	});

	it("Should get correct token when token is set", () => {
		repository.saveToken(expectedToken);
		expect(repository.getToken()).toBe(expectedToken);
	});
});
