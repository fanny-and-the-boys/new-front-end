import { setupServer } from "msw/node";
import handlers from "./handlers";

const API_URL = import.meta.env.VITE_API_TEST_URL;

const server = setupServer(...(await handlers(API_URL)));

export default server;
