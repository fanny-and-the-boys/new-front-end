import { BookTableGatewayResponse } from "@src/domain/ports/server-side/types";
import { CateringService } from "@src/domain/value-objects/enums";
import { rest } from "msw";

export const tableBooked: BookTableGatewayResponse = true;

const booking = {
	nbSeats: 2,
	date: new Date(),
	service: CateringService.Lunch,
	client: {
		name: "John Doe",
		phoneNumber: "(123) 456-7890",
		email: "john.doe@test-email.com",
	},
	restaurantId: "123456789",
	tables: [],
};

export default (API_URL: string) => [
	rest.post(`${API_URL}/booking`, async (_, result, context) => {
		return result(context.status(200));
	}),
	rest.get(`${API_URL}/booking/next`, async (_, result, context) => {
		return result(context.status(200), context.json([booking]));
	}),
];
