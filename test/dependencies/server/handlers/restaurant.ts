import { rest } from "msw";

export const restaurant = {
	id: "123456789",
	name: "Test Restaurant",
	address: {
		id: "123456789",
		street: "Test Street",
		city: "Test City",
		postalCode: "12345",
		country: "Test Country",
	},
	email: "restaurant-email@test-email.com",
	user: {
		id: "123456789",
		login: "TestLogin",
	},
	phoneNumber: "(123) 456-7890",
	description: "Test Description",
	imageUrl: "https://test-image-url.com",
	tables: [],
};

export const restaurantsSimple = [
	{
		id: "1",
		name: "Kebab 2000",
		description: "Super kébab ! ",
		imageUrl: "https://i.ytimg.com/vi/K-A6xtGAXdI/hqdefault.jpg",
	},
	{
		id: "2",
		name: "Le Petit Bar",
		description:
			"Le Gang du Petit Bar est un groupe de criminalité organisée corse",
		imageUrl:
			"https://external-preview.redd.it/tZRi3Xsl3K18ZJeDfr-cN0uu3G-9ZMKUwzMeC3ej-2w.png?width=640&crop=smart&format=pjpg&auto=webp&s=4b2d12357991a7f4282275a1ce83312d9348cd14",
	},
];

export default (API_URL: string) => [
	rest.get(`${API_URL}/restaurant/simple`, async (_, result, context) => {
		return result(context.status(200), context.json(restaurantsSimple));
	}),
	rest.get(`${API_URL}/restaurant/:id`, async (_, result, context) => {
		return result(context.status(200), context.json(restaurant));
	}),
	rest.get(`${API_URL}/restaurant/current`, async (_, result, context) => {
		return result(context.status(200), context.json(restaurant));
	}),
];
