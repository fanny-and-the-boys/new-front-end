import { rest } from "msw";

export const testToken = "qwertyuiopasdfghjklzxcvbnm123456";

export default (API_URL: string) => [
	rest.post(`${API_URL}/user/login`, async (request, result, context) => {
		const { login, password } = await request.json();
		if (login === "TesterLogin" && password === "TesterPassword") {
			return result(
				context.status(200),
				context.json({
					login,
					token: testToken,
				}),
			);
		}
		return result(
			context.status(401),
			context.json({
				message: "Incorrect credentials",
			}),
		);
	}),
];
