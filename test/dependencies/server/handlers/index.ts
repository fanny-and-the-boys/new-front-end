import fs from "fs/promises";

export default async (API_URL: string) => {
	const files = await fs.readdir(__dirname);
	const handlers = await Promise.all(
		files
			.filter((file) => file !== "index.ts")
			.map(async (file) => {
				const handler = await import(`./${file}`);
				return handler.default(API_URL);
			}),
	);
	return handlers.flat();
};
