import { vi } from "vitest";

export const mockedUseNavigate = vi.fn();
export const mockReactRouter = () => {
	vi.mock("react-router-dom", async () => {
		const actual = await vi.importActual<object>("react-router-dom");
		return {
			...actual,
			useNavigate: mockedUseNavigate,
		};
	});
};
