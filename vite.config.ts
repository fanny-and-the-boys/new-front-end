/// <reference types="vitest" />

import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [react()],
	resolve: {
		alias: {
			"@src": path.resolve(__dirname, "src"),
			"@models": path.resolve(__dirname, "src/domain/models"),
			"@ports" : path.resolve(__dirname, "src/domain/ports"),
			"@usecases" : path.resolve(__dirname, "src/domain/usecases"),
			"@providers" : path.resolve(__dirname, "src/providers"),
			"@ui" : path.resolve(__dirname, "src/ui"),
			"@pages" : path.resolve(__dirname, "src/ui/pages"),
			"@components" : path.resolve(__dirname, "src/ui/components"),
			"@test": path.resolve(__dirname, "test"),
		},
	},
	test: {
		globals: true,
		environment: "jsdom",
		setupFiles: ["test/setup.ts"],
	},
});

