import HomePage from "./HomePage";
import BookingPage from "./BookingPage";
import DashboardPage from "./DashboardPage";
import LoginPage from "./LoginPage";
import RestaurantsPage from "./RestaurantPage";
import ErrorPage from "./ErrorPage";

export {
    HomePage as Home,
    BookingPage as Booking,
    DashboardPage as Dashboard,
    LoginPage as Login,
    RestaurantsPage as Restaurant,
    ErrorPage as Error,
};
