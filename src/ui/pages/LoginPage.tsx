import {
	Card,
	CardBody,
	CardHeader,
	Typography,
} from "@material-tailwind/react";
import { LoginUseCase } from "@src/domain/usecases";
import { LoginForm } from "@src/ui/components/login/LoginForm";

type ILoginPageProps = {
	loginUseCase: LoginUseCase;
};

const LoginPage = ({ loginUseCase }: ILoginPageProps) => (
	<div className="flex items-center justify-center h-screen">
		<Card className="w-1/3">
			<CardHeader
				color="blue"
				shadow={false}
				className="m-0 p-6 rounded-b-none text-center w-full"
			>
				<Typography
					className="align-baseline"
					variant="h4"
					color="white"
				>
					Bon appétit ! 👨‍🍳
				</Typography>
			</CardHeader>
			<CardBody className="rounded-t-none w-full">
				<LoginForm loginUseCase={loginUseCase} />
			</CardBody>
		</Card>
	</div>
);

export default LoginPage;
