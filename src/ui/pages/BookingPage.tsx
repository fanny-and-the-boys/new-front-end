import { useContext } from "react";
import {
	Card,
	CardBody,
	CardHeader,
	Typography,
} from "@material-tailwind/react";

import { DateContext } from "@src/providers/DateProvider";
import { bookingUseCase } from "@src/UseCaseFactory";
import { BookingForm, RestaurantPreview } from "@components/booking";

const BookingPage = () => {
	const dateContext = useContext(DateContext);

	if (!dateContext.getYesterday() || dateContext.getYesterday() === null) {
		return <></>; // Render nothing until the context value is available
	}

	const { getYesterday } = dateContext;

	return (
		<div className="grid h-full  grid-cols-1 md:grid-cols-8">
			<div className="md:col-start-2 md:col-span-2 grid place-items-center">
				<Card className="mb-4 md:mb-0">
					<CardHeader
						floated={false}
						shadow={false}
						color="blue"
						className="m-0  rounded-b-none py-8 px-4 text-center"
					>
						<Typography
							className="align-baseline"
							variant="h4"
							color="white"
						>
							Réservation
						</Typography>
					</CardHeader>
					<CardBody>
						<BookingForm
							bookingUseCase={bookingUseCase}
							startDate={getYesterday()}
							disabledDates={[]}
						/>
					</CardBody>
				</Card>
			</div>
			<div className="md:col-span-3 md:col-start-5 md:col-end-8 flex justify-center">
				<div className="max-w-lg flex items-center">
					<RestaurantPreview />
				</div>
			</div>
		</div>
	);
};

export default BookingPage;
