import {
	MenuCard,
	PlanningCard,
	BookingCard,
} from "@src/ui/components/dashboard";

const DashboardPage = () => {
	return (
		<div className=" flex flex-col items-center justify-center h-full w-full p-10">
			<div className="flex flex-row items-center justify-center h-full w-full m-5 gap-5">
				<MenuCard />
				<PlanningCard />
			</div>
			<div className="flex flex-col items-center justify-center h-full w-full m-5">
				<BookingCard />
			</div>
		</div>
	);
};

export default DashboardPage;
