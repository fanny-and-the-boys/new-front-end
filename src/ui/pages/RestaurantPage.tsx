import { Typography } from "@material-tailwind/react";

import { RestaurantList } from "@src/ui/components/restaurants";

const RestaurantsPage = () => {
	return (
		<div>
			<Typography className="text-center pb-6" variant="h2">
				Restaurants
			</Typography>
			<div className="flex justify-center">
				<RestaurantList />
			</div>
		</div>
	);
};

export default RestaurantsPage;