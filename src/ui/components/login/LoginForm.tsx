import { useForm } from "react-hook-form";
import { Button, Input, Typography } from "@material-tailwind/react";
import { ILoginUseCase } from "@src/domain/usecases";
import { LoginGatewayRequest } from "@src/domain/ports/server-side/types";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

interface ILoginFormProps {
	loginUseCase: ILoginUseCase;
}

export const LoginForm = ({ loginUseCase }: ILoginFormProps) => {
	const navigate = useNavigate();
	const [loginSuccess, setLoginSuccess] = useState<boolean>();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<LoginGatewayRequest>();


	const onSubmit = handleSubmit(async (data) => {
		setLoginSuccess(undefined);
		const success = await loginUseCase.login(data);
		if (success) {
			navigate("/dashboard");
		} else {
			setLoginSuccess(false);
		}
	});

	return (
		<form onSubmit={onSubmit} className="flex flex-col gap-4">
			<Input
				id="login"
				data-testid="login"
				type="text"
				size="lg"
				aria-label="login"
				label="Login"
				{...register("login", { required: true })}
				error={!!errors.login}
			/>
			<Input
				id="password"
				data-testid="password"
				type="password"
				size="lg"
				aria-label="password"
				label="Password"
				{...register("password", { required: true })}
				error={!!errors.password}
			/>
			<Button color="blue" type="submit" ripple={true}>
				Login
			</Button>
			{loginSuccess === false && (
				<div data-testid="error-message">
					<Typography variant="small" color="red">
						Impossible de se connecter, vérifiez votre identifiant /
						mot de passe
					</Typography>
				</div>
			)}
		</form>
	);
};
