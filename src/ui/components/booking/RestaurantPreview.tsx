import {
	Card,
	CardBody,
	CardHeader,
	Typography,
} from "@material-tailwind/react";
import { Restaurant } from "@src/domain/models";
import { useLoaderData } from "react-router-dom";

interface RestaurantPreviewProps {
	restaurant?: Restaurant;
}

export const RestaurantPreview = ({ restaurant }: RestaurantPreviewProps) => {
	const loaderData = useLoaderData();

	if (!restaurant) {
		restaurant = loaderData as Restaurant;
	}
	return (
		<Card>
			<CardHeader
				floated={false}
				shadow={false}
				color="blue"
				className="m-0  rounded-b-none py-8 px-4 text-center"
			>
				<Typography
					className="align-baseline"
					variant="h4"
					color="white"
				>
					{restaurant.name}
				</Typography>
			</CardHeader>
			<CardBody className="pb-4">
				<img
					className="rounded-lg mb-3"
					src={restaurant.imageUrl}
					alt="Restaurant Image"
				/>
				<Typography
					className="align-baseline"
					variant="medium"
					color="blue-gray"
				>
					{restaurant.description}
				</Typography>
			</CardBody>
		</Card>
	);
};
