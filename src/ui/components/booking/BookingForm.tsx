import { useState } from "react";
import { useForm } from "react-hook-form";
import { useLoaderData } from "react-router-dom";
import {
	Button,
	Input,
	Tab,
	Tabs,
	TabsHeader,
	Typography,
} from "@material-tailwind/react";
import { Restaurant } from "@src/domain/models";
import { IBookingUseCase } from "@src/domain/usecases";
import { BookingRequest } from "@src/domain/ports/server-side/types";
import { CateringService } from "@src/domain/value-objects/enums";
import Datepicker from "react-tailwindcss-datepicker";
import { DateRangeType } from "react-tailwindcss-datepicker/dist/types";
import { BookingConfirmationMessage } from "@components/booking/BookingConfirmationMessage.tsx";

interface IBookingFormProps {
	bookingUseCase: IBookingUseCase;
	disabledDates: DateRangeType[];
	restaurant?: Restaurant;
	startDate: Date;
}

export const BookingForm = (props: IBookingFormProps) => {
	const { bookingUseCase } = props;
	let { restaurant } = props;

	const loaderData = useLoaderData();
	if (!restaurant) {
		restaurant = loaderData as Restaurant;
	}

	const [bookingConfirmed, setBookingConfirmed] = useState<boolean>();
	const { register, handleSubmit } = useForm<BookingRequest>();
	const [service, setService] = useState(CateringService.Lunch);
	const [dateRange, setDateRange] = useState({
		startDate: null,
		endDate: null,
	});

	const bookTable = handleSubmit((bookingInfo: BookingRequest) => {
		setBookingConfirmed(undefined);
		if (!restaurant) return;
		bookingInfo.restaurantId = restaurant.id;
		bookingUseCase.bookTable(bookingInfo).then((response) => {
			setBookingConfirmed(response);
		});
	});

	register("service", { value: 0 });
	return (
		<form
			className="max-w-screen-lg sm:w-96 flex flex-col gap-4"
			aria-label="form"
			onSubmit={bookTable}
		>
			<Input
				size="lg"
				type="text"
				label="A  quel nom ?"
				id="name"
				required={true}
				{...register("client.name")}
			/>
			<Input
				size="lg"
				type="text"
				label="Avec quel email ?"
				id="email"
				{...register("client.email")}
				required={true}
			/>
			<Input
				size="lg"
				type="text"
				label="Quel numéro ?"
				id="phone-number"
				required={true}
				{...register("client.phoneNumber")}
			/>
			<Input //IDEA : could be interesting to have a number picker
				size="lg"
				type="number"
				role="textbox"
				aria-label="" //TOCHECK need in order to pass the tests
				label="Pour combien ?"
				id="seats"
				required={true}
				{...register("nbSeats", {
					valueAsNumber: true,
				})}
			/>

			<div id="datepicker-wrapper">
				<Datepicker
					asSingle={true}
					useRange={false}
					value={dateRange}
					startWeekOn="mon"
					minDate={props.startDate}
					placeholder="Pour quand ?"
					displayFormat={"DD/MM/YYYY"}
					onChange={(value: any) => {
						setDateRange(value);
						register("date", {
							value: value.startDate,
							required: true,
						});
					}}
					disabledDates={props.disabledDates}
					i18n={"fr"}
				/>
			</div>

			<Typography variant="small">Pour quel service ?</Typography>
			<Tabs value={service} className="overflow-visible">
				<TabsHeader className="relative z-0 ">
					{Object.values(CateringService).map((service, key) => (
						<Tab
							key={service}
							value={service}
							onClick={() => {
								setService(service);
								register("service", { value: key });
							}}
						>
							{service}
						</Tab>
					))}
				</TabsHeader>
			</Tabs>
			<Button size="lg" className="mt-2" type="submit">
				Réserver
			</Button>
			<BookingConfirmationMessage booking={bookingConfirmed} />
		</form>
	);
};
