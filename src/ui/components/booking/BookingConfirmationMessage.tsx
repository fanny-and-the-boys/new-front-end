import { Typography } from "@material-tailwind/react";

interface IBookingConfirmationMessageProps {
	booking: boolean | undefined;
}

export const BookingConfirmationMessage = ({
	booking,
}: IBookingConfirmationMessageProps) => {
	if (booking === true) {
		return (
			<div data-testid="booking-success-message">
				<Typography variant="small" color="green">
					Merci ! La réservation a bien été prise en compte.
				</Typography>
			</div>
		);
	}
	if (booking === false) {
		return (
			<div data-testid="booking-error-message">
				<Typography variant="small" color="red">
					Impossible de réserver, veuillez réessayer plus tard.
				</Typography>
			</div>
		);
	}
	return null;
};
