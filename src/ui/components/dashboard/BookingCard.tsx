import { Card, CardBody, Typography } from "@material-tailwind/react";
import { BookingGuests, BookingTables } from "./bookings";

export const BookingCard = () => {
	return (
		<Card className="flex flex-col items-center justify-center w-full h-full p-5">
			<CardBody className="w-full h-full">
				<Typography className="align-baseline text-center" variant="h4">
					Bookings
				</Typography>
				<div className="flex flex-row items-center justify-center w-full h-full">
					<BookingGuests />
					<BookingTables />
				</div>
			</CardBody>
		</Card>
	);
};
