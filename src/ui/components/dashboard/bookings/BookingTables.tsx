import { Typography } from "@material-tailwind/react";
import { Booking } from "@src/domain/models";
import { useEffect, useState } from "react";
import { useLoaderData } from "react-router-dom";

interface IBookingTablesProps {
	bookings?: Booking[];
}

export const BookingTables = ({ bookings }: IBookingTablesProps) => {
	const loaderData = useLoaderData();
	if (!bookings) {
		bookings = loaderData as Booking[];
	}
	const [bookingsByNbSeats, setBookingsByNbSeats] = useState<Array<number>>(
		[],
	);
	useEffect(() => {
		if (!bookings) return;
		setBookingsByNbSeats(
			bookings.reduce((acc, curr) => {
				if (!acc.includes(curr.nbSeats)) {
					acc.push(curr.nbSeats);
				}
				return acc;
			}, [] as Array<number>),
		);
	}, [bookings]);

	return (
		<div className="flex flex-col items-center justify-center w-1/2 h-full">
			<Typography className="align-baseline text-center" variant="h5">
				Tables
			</Typography>
			{!!bookings && bookings.length ? (
				<div
					className="flex flex-col items-center justify-center w-full h-full"
					data-testid="bookingTables"
				>
					<ul className="flex flex-col items-center justify-center w-full h-full">
						{bookingsByNbSeats.map((nbSeats) => (
							<li
								className="flex flex-row items-center justify-center w-full h-full"
								key={nbSeats}
								data-testid={`tables-${nbSeats}`}
							>
								<Typography className="flex flex-row items-center justify-center align-baseline text-center grow">
									Table of {nbSeats}:{" "}
									{
										bookings?.filter(
											(reservation) =>
												reservation.nbSeats === nbSeats,
										).length
									}
								</Typography>
							</li>
						))}
					</ul>
					<Typography className="flex flex-row items-center justify-center align-baseline text-center grow">
						Total of tables: {bookings.length}
					</Typography>
				</div>
			) : (
				<Typography
					className="flex flex-row items-center justify-center align-baseline text-center grow"
					data-testid="no-bookings"
				>
					No tables booked
				</Typography>
			)}
		</div>
	);
};
