import { Typography } from "@material-tailwind/react";
import { Booking } from "@src/domain/models";
import { useLoaderData } from "react-router-dom";

interface IBookingGuestsProps {
	bookings?: Booking[];
}

export const BookingGuests = ({ bookings }: IBookingGuestsProps) => {
	const loaderData = useLoaderData();
	if (!bookings) {
		bookings = loaderData as Booking[];
	}
	return (
		<div className="flex flex-col w-1/2 h-full" data-testid="bookingGuests">
			<Typography className="align-baseline text-center" variant="h5">
				Guests
			</Typography>
			<Typography
				className="flex flex-row items-center justify-center align-baseline text-center grow"
				variant="h1"
			>
				{!!bookings &&
					bookings.reduce((acc, curr) => acc + curr.nbSeats, 0)}
			</Typography>
		</div>
	);
};
