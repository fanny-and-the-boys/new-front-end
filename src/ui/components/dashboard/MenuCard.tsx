import { Card, CardBody, Typography } from "@material-tailwind/react";

export const MenuCard = () => {
	return (
		<Card className="flex flex-col items-center justify-center w-1/2 h-full">
			<CardBody className="w-full h-full">
				<Typography className="align-baseline text-center" variant="h4">
					Menu
				</Typography>
			</CardBody>
		</Card>
	);
};
