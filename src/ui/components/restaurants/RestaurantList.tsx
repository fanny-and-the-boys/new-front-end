import { RestaurantCard } from "@src/ui/components/restaurants";
import { RestaurantSimple } from "@src/domain/models";
import { useLoaderData } from "react-router-dom";

interface IRestaurantListProps {
	restaurants?: RestaurantSimple[];
}

export const RestaurantList = ({ restaurants }: IRestaurantListProps) => {
	const loaderData = useLoaderData();
	if (!restaurants) {
		restaurants = loaderData as RestaurantSimple[];
	}
	return !!restaurants && restaurants.length ? (
		<div
			className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8"
			data-testid="restaurants"
		>
			{restaurants.map((restaurant) => (
				<RestaurantCard key={restaurant.id} restaurant={restaurant} />
			))}
		</div>
	) : (
		<div
			className="flex flex-row items-center justify-center align-baseline text-center grow"
			data-testid="no-restaurants"
		>
			No restaurants found
		</div>
	);
};
