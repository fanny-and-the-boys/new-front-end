import {
	Card,
	CardBody,
	CardHeader,
	Typography,
} from "@material-tailwind/react";
import { RestaurantSimple } from "@src/domain/models";
import { Link } from "react-router-dom";

interface IRestaurantCardProps {
	restaurant: RestaurantSimple;
}

export const RestaurantCard = ({ restaurant }: IRestaurantCardProps) => {
	return (
		<Card
			className="w-[24rem] h-[16rem] overflow-hidden"
			data-testid="restaurant-card"
		>
			<CardHeader
				floated={false}
				shadow={false}
				color="transparent"
				className="m-0 rounded-none"
			>
				<img
					data-testid="restaurant-img"
					src={restaurant.imageUrl}
					alt="Restaurant Image"
				/>
			</CardHeader>
			<CardBody>
				<Link to={`/book/${restaurant.id}`}>
					<Typography
						data-testid="restaurant-name"
						variant="h4"
						color="blue-gray"
					>
						{restaurant.name}
					</Typography>
				</Link>
			</CardBody>
		</Card>
	);
};
