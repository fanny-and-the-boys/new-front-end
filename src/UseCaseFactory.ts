import { JWTRepository } from "@src/adapters/repositories";
import {
	BookingGateway,
	LoginGateway,
	RestaurantGateway,
} from "@src/adapters/gateways";

import {
	BookingUseCase,
	LoginUseCase,
	RestaurantUseCase,
} from "@src/domain/usecases";

// User-Side adapters
const jwtRepository = new JWTRepository();

// Server-side adapters
const loginGateway = new LoginGateway();
const bookingGateway = new BookingGateway();
const restaurantGateway = new RestaurantGateway();

// Use cases
const loginUseCase = new LoginUseCase(loginGateway, jwtRepository);
const bookingUseCase = new BookingUseCase(bookingGateway, jwtRepository);
const restaurantUseCase = new RestaurantUseCase(restaurantGateway);

export { loginUseCase, bookingUseCase, restaurantUseCase };
