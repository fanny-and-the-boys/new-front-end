import { IJWTRepository } from "@src/domain/ports/user-side";

export class JWTRepository implements IJWTRepository {
	saveToken(token: string): void {
		localStorage.setItem("token", token);
	}

	getToken(): string {
		const token = localStorage.getItem("token");
		if (!token) {
			throw new Error("JWT token is not set");
		}
		return token;
	}
}
