import axios from "axios";

import { Restaurant } from "@src/domain/models";
import { IRestaurantGateway } from "src/domain/ports/server-side";
import {
	GetAllRestaurantsResponse,
	GetConnectedUserRestaurantRequest,
	GetConnectedUserRestaurantResponse,
	GetRestaurantByIdRequest,
	GetRestaurantByIdResponse,
} from "@src/domain/ports/server-side/types";
import { API_BASE_URL } from "@src/adapters/gateways/api";

export class RestaurantGateway implements IRestaurantGateway {
	public async getConnectedUserRestaurant(
		props: GetConnectedUserRestaurantRequest,
	): Promise<GetConnectedUserRestaurantResponse> {
		const { data } = await axios.get<Restaurant>(
			`${API_BASE_URL}/restaurant/current`,
			{
				headers: {
					Authorization: `Bearer ${props}`,
				},
			},
		);
		return data;
	}

	public async getAllRestaurants(): Promise<GetAllRestaurantsResponse> {
		const { data } = await axios.get(`${API_BASE_URL}/restaurant/simple`);
		return data;
	}

	public async getRestaurantById(
		props: GetRestaurantByIdRequest,
	): Promise<GetRestaurantByIdResponse> {
		const { data } = await axios.get(`${API_BASE_URL}/restaurant/${props}`);
		return data;
	}
}
