import axios from "axios";

import { IBookingGateway } from "@src/domain/ports/server-side";
import {
	BookingRequest,
	BookTableGatewayResponse,
	GetNextBookingListGatewayResponse,
} from "@src/domain/ports/server-side/types";
import { API_BASE_URL, validateStatus } from "@src/adapters/gateways/api";

export class BookingGateway implements IBookingGateway {
	public async getNextBookings(
		token: string,
	): Promise<GetNextBookingListGatewayResponse> {
		const { data } = await axios.get<GetNextBookingListGatewayResponse>(
			`${API_BASE_URL}/booking/next`,
			{
				headers: {
					Authorization: `Bearer ${token}`,
				},
			},
		);
		return data;
	}

	public async bookTable(
		props: BookingRequest,
	): Promise<BookTableGatewayResponse> {
		try {
			const response = await axios.post<BookTableGatewayResponse>(
				`${API_BASE_URL}/booking`,
				props,
			);
			return validateStatus(response.status);
		} catch (error) {
			console.log(error);
			return false;
		}
	}
}
