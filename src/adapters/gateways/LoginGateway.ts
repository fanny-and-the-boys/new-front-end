import axios from "axios";

import { ILoginGateway } from "@src/domain/ports/server-side";
import {
	LoginGatewayRequest,
	LoginGatewayResponse,
} from "@src/domain/ports/server-side/types";
import { API_BASE_URL } from "@src/adapters/gateways/api";

export class LoginGateway implements ILoginGateway {
	async login(login: LoginGatewayRequest): Promise<LoginGatewayResponse> {
		const { data } = await axios.post<LoginGatewayResponse>(
			`${API_BASE_URL}/user/login`,
			login,
		);
		return data;
	}
}
