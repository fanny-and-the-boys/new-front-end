import React, { createContext, useEffect } from "react";

interface IDateContext {
    getCurrentDate: () => Date;
    getYesterday: () => Date;
}

export const DateContext = createContext<IDateContext>({} as IDateContext);

export function DateProvider({ children, ...props } : any) {
    const [contextValue, setContextValue] = React.useState<IDateContext>({} as IDateContext);

    useEffect(() => {
        const isTestMode = import.meta.env.MODE  === 'testing';

        const getCurrentDate = () : Date => {
            return ( isTestMode ? new Date('02/01/1999') :  new Date() );
        }
        const getYesterday = (): Date => {
            const currentDate = getCurrentDate();
            const yesterday = new Date(currentDate);
            yesterday.setDate(currentDate.getDate() - 1);
            
            return yesterday;
        }
        setContextValue({
            getCurrentDate,
            getYesterday
        } as IDateContext);
        return () => {};
    }, []); // Empty dependency array ensures the effect runs only once during initialization

    return (
        <DateContext.Provider value = { contextValue }>
            {children}
        </DateContext.Provider>
    );
}