import { createBrowserRouter } from "react-router-dom";
import "./index.css";

import * as pages from "@src/ui/pages";

import {
	bookingUseCase,
	loginUseCase,
	restaurantUseCase,
} from "./UseCaseFactory";


const BrowserRouter = createBrowserRouter([
	{
		path: "/",
		element: <pages.Home />,
		errorElement: <pages.Error />,
	},
	{
		path: "login",
		element: <pages.Login loginUseCase={loginUseCase} />,
		errorElement: <pages.Error />,
	},
	{
		path: "dashboard",
		element: <pages.Dashboard />,
		loader: async () => {
			return bookingUseCase.getNextBookings();
		},
		errorElement: <pages.Error />,
	},
	{
		path: "restaurants",
		element: <pages.Restaurant />,
		loader: async () => {
			return restaurantUseCase.getAllRestaurants();
		},
		errorElement: <pages.Error />,
	},
	{
		path: "book/:id",
		element: <pages.Booking />,
		loader: async ({ params: { id } }) => {
			if (!id) throw new Error("No id");
			return restaurantUseCase.getRestaurantById(id);
		},
		errorElement: <pages.Error />,
	},
	{
		path: "*",
		element: <pages.Error />,
	},
]);

export default BrowserRouter;
