import { StrictMode } from "react";
import { RouterProvider } from "react-router-dom";
import { createRoot } from "react-dom/client";
import { ThemeProvider } from "@material-tailwind/react";
import BrowserRouter from "./router";
import "./index.css";
import { DateProvider } from "@providers/DateProvider";

createRoot(document.getElementById("root") as HTMLElement).render(
	<StrictMode>
		<ThemeProvider>
			<DateProvider>
				<RouterProvider router={BrowserRouter} />
			</DateProvider>
		</ThemeProvider>
	</StrictMode>,
);

