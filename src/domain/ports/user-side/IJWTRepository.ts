export interface IJWTRepository {
	saveToken(token: string): void;
	getToken(): string;
}
