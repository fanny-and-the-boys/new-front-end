import {
	LoginGatewayRequest,
	LoginGatewayResponse,
} from "@src/domain/ports/server-side/types";

export interface ILoginGateway {
	login(props: LoginGatewayRequest): Promise<LoginGatewayResponse>;
}
