export type { IBookingGateway } from "./IBookingGateway";
export type { ILoginGateway } from "./ILoginGateway";
export type { IRestaurantGateway } from "./IRestaurantGateway";
