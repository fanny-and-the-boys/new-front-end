import {
	BookingRequest,
	BookTableGatewayResponse,
	GetNextBookingListGatewayResponse,
} from "@src/domain/ports/server-side/types";

export interface IBookingGateway {
	bookTable(props: BookingRequest): Promise<BookTableGatewayResponse>;

	getNextBookings(token: string): Promise<GetNextBookingListGatewayResponse>;
}
