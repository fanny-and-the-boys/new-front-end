import {
	GetAllRestaurantsResponse,
	GetConnectedUserRestaurantRequest,
	GetConnectedUserRestaurantResponse,
	GetRestaurantByIdRequest,
	GetRestaurantByIdResponse,
} from "@src/domain/ports/server-side/types";

export interface IRestaurantGateway {
	getAllRestaurants(): Promise<GetAllRestaurantsResponse>;
	getRestaurantById(
		props: GetRestaurantByIdRequest,
	): Promise<GetRestaurantByIdResponse>;
	getConnectedUserRestaurant(
		props: GetConnectedUserRestaurantRequest,
	): Promise<GetConnectedUserRestaurantResponse>;
}
