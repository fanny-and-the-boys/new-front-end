import { Booking, isClient } from "@src/domain/models";
import { WithoutId } from "@src/domain/ports/server-side/types";

export type BookingRequest = {
	nbSeats: Booking["nbSeats"];
	client: WithoutId<Booking["client"]>;
	restaurantId: string;
	date: Booking["date"];
	service: number;
};

export const isBookingRequest = (obj: unknown): obj is BookingRequest => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const { nbSeats, client, restaurantId, date } = obj as BookingRequest;

	if (!isClient(client) || 
		typeof nbSeats !== "number" ||
		typeof restaurantId !== "string" ||
		date === undefined)
		return false;
	return true;
};

export type BookTableGatewayResponse = boolean;

export type GetNextBookingListGatewayResponse = Booking[];
