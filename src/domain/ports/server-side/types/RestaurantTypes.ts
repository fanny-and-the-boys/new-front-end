import { Restaurant, RestaurantSimple } from "@src/domain/models";

export type GetAllRestaurantsResponse = RestaurantSimple[];

export type GetRestaurantByIdRequest = string;

export type GetRestaurantByIdResponse = Restaurant;

export type GetConnectedUserRestaurantRequest = string;

export type GetConnectedUserRestaurantResponse = Restaurant;
