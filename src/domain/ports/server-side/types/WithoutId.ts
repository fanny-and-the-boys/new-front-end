export type WithoutId<T> = {
	[K in keyof T as Exclude<K, "id">]: T[K];
};
