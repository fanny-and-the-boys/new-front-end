export interface LoginGatewayRequest {
	login: string;
	password: string;
}

export interface LoginGatewayResponse {
	token: string;
	login: string;
}
