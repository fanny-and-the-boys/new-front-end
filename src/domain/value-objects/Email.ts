export type Email = string;

export const isEmail = (value: unknown): value is Email => {
	if (typeof value !== "string") {
		return false;
	}
	const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	return emailPattern.test(value);
};
