export type PhoneNumber = string;

export const isPhoneNumber = (value: unknown): value is PhoneNumber => {
	if (typeof value !== "string") {
		return false;
	}
	const cleanedNumber = value.replace(/\D/g, "");
	const phoneNumberPattern = /^\d{10}$/;
	return phoneNumberPattern.test(cleanedNumber);
};
