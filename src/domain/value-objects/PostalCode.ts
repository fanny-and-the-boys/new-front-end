export type PostalCode = string;

export const isPostalCode = (value: unknown): value is PostalCode => {
	if (typeof value !== "string") {
		return false;
	}
	const postalCodePattern = /^[0-9]{5}(?:-[0-9]{4})?$/;
	return postalCodePattern.test(value);
};
