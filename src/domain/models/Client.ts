import {
	PhoneNumber,
	Email,
	isEmail,
	isPhoneNumber,
} from "@src/domain/value-objects";

export interface Client {
	id?: string;
	name: string;
	phoneNumber: PhoneNumber;
	email: Email;
}

export const isClient = (obj: unknown): obj is Client => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const { id, name, phoneNumber, email } = obj as Client;

	if (id && typeof id !== "string") {
		return false;
	}
	if (typeof name !== "string") {
		return false;
	}
	if (!isPhoneNumber(phoneNumber)) {
		return false;
	}
	if (!isEmail(email)) {
		return false;
	}
	return true;
};
