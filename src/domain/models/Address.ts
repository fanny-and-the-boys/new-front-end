import { PostalCode, isPostalCode } from "@src/domain/value-objects";

export interface Address {
	id: string;
	street: string;
	city: string;
	postalCode: PostalCode;
	country: string;
}

export const isAddress = (obj: unknown): obj is Address => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const { id, street, city, postalCode, country } = obj as Address;

	if (typeof id !== "string") {
		return false;
	}
	if (typeof street !== "string") {
		return false;
	}
	if (typeof city !== "string") {
		return false;
	}
	if (!isPostalCode(postalCode)) {
		return false;
	}
	if (typeof country !== "string") {
		return false;
	}
	return true;
};
