export interface Table {
	id: string;
	number: number;
	nbSeats: number;
}
