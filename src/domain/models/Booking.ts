import {
	Table,
	Client,
	Restaurant,
	isClient,
	isRestaurant,
} from "@src/domain/models";
import { CateringService } from "@src/domain/value-objects/enums";

export interface Booking {
	id: string;
	nbSeats: number;
	date: Date;
	service: CateringService;
	tables: Table[];
	client: Client;
	restaurant: Restaurant;
}

export const isBooking = (obj: unknown): obj is Booking => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const { id, nbSeats, client, restaurant } = obj as Booking;

	if (typeof id !== "string") {
		return false;
	}
	if (typeof nbSeats !== "number") {
		return false;
	}
	if (!isClient(client)) {
		return false;
	}
	if (!isRestaurant(restaurant)) {
		return false;
	}
	return true;
};
