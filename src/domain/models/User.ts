export interface User {
	id: string;
	login: string;
}

export const isUser = (obj: unknown): obj is User => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const { id, login } = obj as User;

	if (typeof id !== "string") {
		return false;
	}
	if (typeof login !== "string") {
		return false;
	}
	return true;
};
