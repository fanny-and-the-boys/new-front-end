import { Address, isAddress, isUser, Table, User } from "@src/domain/models";
import {
	Email,
	isEmail,
	isPhoneNumber,
	PhoneNumber,
} from "@src/domain/value-objects";

export interface RestaurantSimple {
	id: string;
	name: string;
	imageUrl: string;
}

export interface Restaurant extends RestaurantSimple {
	user: User;
	address: Address;
	phoneNumber: PhoneNumber;
	email: Email;
	description: string;
	tables: Table[];
}

export const isRestaurant = (obj: unknown): obj is Restaurant => {
	if (typeof obj !== "object" || obj === null) {
		return false;
	}
	const {
		id,
		user,
		name,
		address,
		phoneNumber,
		email,
		imageUrl,
		description,
	} = obj as Restaurant;

	if (typeof id !== "string") {
		return false;
	}
	if (!isUser(user)) {
		return false;
	}
	if (typeof name !== "string") {
		return false;
	}
	if (!isAddress(address)) {
		return false;
	}
	if (!isPhoneNumber(phoneNumber)) {
		return false;
	}
	if (!isEmail(email)) {
		return false;
	}
	if (typeof imageUrl !== "string") {
		return false;
	}
	if (typeof description !== "string") {
		return false;
	}
	return true;
};
