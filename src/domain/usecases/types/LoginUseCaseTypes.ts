import { LoginGatewayRequest } from "@src/domain/ports/server-side/types";

export type LoginUseCaseRequest = LoginGatewayRequest;

export type LoginUseCaseResponse = boolean;
