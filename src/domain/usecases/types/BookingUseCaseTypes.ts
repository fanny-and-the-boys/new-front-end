import { GetNextBookingListGatewayResponse } from "@src/domain/ports/server-side/types";

export type BookTableUseCaseResponse = boolean;

export type GetNextBookingsUseCaseResponse = GetNextBookingListGatewayResponse;
