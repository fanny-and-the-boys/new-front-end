import { IBookingGateway } from "@src/domain/ports/server-side";
import {
	BookTableUseCaseResponse,
	GetNextBookingsUseCaseResponse,
} from "./types/BookingUseCaseTypes";
import { BookingRequest, isBookingRequest } from "@ports/server-side/types";
import { IJWTRepository } from "@ports/user-side";

export interface IBookingUseCase {
	bookTable(props: BookingRequest): Promise<BookTableUseCaseResponse>;

	getNextBookings(): Promise<GetNextBookingsUseCaseResponse>;
}

export class BookingUseCase implements IBookingUseCase {
	constructor(
		private readonly bookingGateway: IBookingGateway,
		private readonly jwtRepository: IJWTRepository,
	) {}

	async bookTable(props: BookingRequest): Promise<BookTableUseCaseResponse> {
		if (!isBookingRequest(props)) {
			return false;
		}
		return this.bookingGateway.bookTable(props);
	}

	async getNextBookings(): Promise<GetNextBookingsUseCaseResponse> {
		const token = this.jwtRepository.getToken();
		return this.bookingGateway.getNextBookings(token);
	}
}
