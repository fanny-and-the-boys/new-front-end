import { ILoginGateway } from "@src/domain/ports/server-side";
import { IJWTRepository } from "@src/domain/ports/user-side";
import {
	LoginUseCaseRequest,
	LoginUseCaseResponse,
} from "./types/LoginUseCaseTypes";

export interface ILoginUseCase {
	login(props: LoginUseCaseRequest): Promise<LoginUseCaseResponse>;
}

export class LoginUseCase implements ILoginUseCase {
	constructor(
		private readonly loginGateway: ILoginGateway,
		private readonly jwtRepository: IJWTRepository,
	) {}
	async login(props: LoginUseCaseRequest): Promise<LoginUseCaseResponse> {
		try {
			const { token } = await this.loginGateway.login(props);
			this.jwtRepository.saveToken(token);
			return true;
		} catch (error) {
			return false;
		}
	}
}
