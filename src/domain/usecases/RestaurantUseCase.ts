import {
	GetAllRestaurantsResponse,
	GetRestaurantByIdRequest,
	GetRestaurantByIdResponse,
} from "@src/domain/ports/server-side/types";
import { IRestaurantGateway } from "@src/domain/ports/server-side";

export interface IRestaurantUseCase {
	getAllRestaurants(): Promise<GetAllRestaurantsResponse>;
	getRestaurantById(
		props: GetRestaurantByIdRequest,
	): Promise<GetRestaurantByIdResponse>;
}

export class RestaurantUseCase implements IRestaurantUseCase {
	constructor(private readonly restaurantGateway: IRestaurantGateway) {}

	async getAllRestaurants(): Promise<GetAllRestaurantsResponse> {
		return await this.restaurantGateway.getAllRestaurants();
	}

	async getRestaurantById(
		props: GetRestaurantByIdRequest,
	): Promise<GetRestaurantByIdResponse> {
		return await this.restaurantGateway.getRestaurantById(props);
	}
}
