import { defineConfig } from "cypress";

export default defineConfig({
	env: {
		apiUrl: "https://reagtu4yewikyeab3ocwn4dhiy0vbjtc.lambda-url.eu-central-1.on.aws/api",
	},
	e2e: {
		baseUrl: "http://localhost:5173/",
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
});
