describe("Restaurant List", () => {
	it("As as client, I can see the restaurants where I can make a reservation", () => {
		// Given a user visits the restaurants page
		cy.visit("/restaurants");

		// Then the user should see a list of restaurants
		cy.get('[data-testid="restaurants"]').should(
			"have.length.greaterThan",
			0,
		);

		// And each restaurant should have a name and an image
		cy.get('[data-testid="restaurant-card"]')
			.first()
			.within(() => {
				cy.get('[data-testid="restaurant-name"]').should("be.visible");
				cy.get('[data-testid="restaurant-img"]').should("be.visible");
			});
	});

	it("As a client, I can click to choose a restaurant to book a table", () => {
		// Given a user visits the restaurants page
		cy.visit("/restaurants");

		// When the user clicks on a given restaurant
		cy.get('[data-testid="restaurant-card"]')
			.first()
			.within(() => {
				cy.get('[data-testid="restaurant-name"]').click();
			});

		// Then the user should be redirected to the booking page
		cy.url().should("include", "/book/");
	});
});
