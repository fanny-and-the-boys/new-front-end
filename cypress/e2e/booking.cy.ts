describe("Booking Page", () => {
	it("As a client, I can book a table", () => {
		cy.fixture("booking.json").then((booking) => {
			cy.intercept("POST", `${Cypress.env("apiUrl")}/booking`).as(
				"postBooking",
			);

			// Given a user visits the booking page of a given restaurant
			cy.visit(`/book/${booking.restaurantId}`);

			// When the user fills and submits the form
			cy.get("#name").type(booking.clientName);
			cy.get("#email").type(booking.clientEmail);
			cy.get("#phone-number").type(booking.clientPhone);
			cy.get("#seats").type(booking.seats);
			cy.get("#datepicker-wrapper input").type(booking.date);
			cy.get("button[type=submit]").click();

			// Then the user should see a confirmation message
			cy.get('[data-testid="booking-success-message"]').should(
				"be.visible",
			);

			cy.wait("@postBooking").then((interception) => {
				const bookingId = interception.response.body.id;
				deleteBooking(bookingId);
			});
		});
	});
});

const deleteBooking = (bookingId: string) => {
	cy.request({
		method: "DELETE",
		url: `${Cypress.env("apiUrl")}/booking/${bookingId}`,
	});
};
