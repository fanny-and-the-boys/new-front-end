describe("The Login Page", () => {
	it("As a restaurant owner I can log in to see my dashboard", () => {
		cy.fixture("credentials.json").then((credentials) => {
			// Given a user visits the login page
			cy.visit("/login");

			// When the user fill and submits the form
			cy.get("input[name=login]").type(credentials.login);
			cy.get("input[name=password]").type(credentials.password);
			cy.get("button[type=submit]").click();

			// Then the user should be redirected to the dashboard
			cy.url().should("include", "/dashboard");
		});
	});
});
